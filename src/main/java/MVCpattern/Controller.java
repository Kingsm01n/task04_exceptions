package MVCpattern;

import Exceptions.MyException;
import Exceptions.MyException2;

public class Controller implements AutoCloseable{
    public void close() throws Exception{
        try {
            throw new MyException();
        } catch(MyException e) {
            throw new MyException2();
        }
    }
}
